function echo(msg: string, err = false) {

  return new Promise((resolve, reject) => {
    setTimeout(() => {
      (!err) ?
        resolve(msg) : reject('ups..')
    }, 1000)
  })
}

const p = echo('lubie')
  .then(x => echo(x + ' placki'))
  .then(x => Promise.reject('Not today...'))
  .then(x => echo(x + ' z kremem'), () => 'From cache')
  .catch(() => 'No conenction')
  .then(console.log)
  .finally(() => { })

