// /// <reference types="" />
import Router from 'express-promise-router'
import passport from 'passport'

export const authRoutes = Router()
  // .post('/login', (req, res, next) => {
  //   if (req.body.password == 'pa55word') {
  //     res.send('OK')
  //   }
  // })


  // .post('/login', passport.authenticate('local', {
  //   successRedirect: '/',
  //   failureRedirect: '/login'
  // }))
  .post('/login', passport.authenticate('local', {}), (req, res, next) => {
    // res.user 
    res.send(req.user)
  })

  .post('/logout', (req, res, next) => {

  })

  .get('/session', (req, res, next) => {

    if (req.session?.id) {
      req.session.views = ++req.session.views || 1;
    }

    res.send({
      msg: 'OK',
      session: req.session,
      user: req.user
    })
  })