import express from 'express'
import { usersService } from '../services'
import sinon from 'sinon'
import supertest from 'supertest'
import { usersRoutes } from './users';

describe('User routes', () => {
  let agent: supertest.SuperAgentTest

  beforeEach(() => {
    const mockResp = sinon.fake.resolves([123])
    sinon.replace(usersService, 'getUsers', mockResp)
    const app = express()
    app.use('/users', usersRoutes)
    agent = supertest.agent(app, {})
  })

  afterEach(() => { sinon.restore() })

  it('get users route', async (/* done */) => {
    return agent.get('/users/')
      .expect('Content-Type', 'application/json; charset=utf-8')
      .expect(200, [123])
      .expect((res) => {
        // expect(sinon.fake()).to.have.been.calledWith()
        expect(res.body).to.deep.equal([123])
        // done(err)
      })
    // .end(done)
  });

  it('get user by id', async () => {
    const userID = '123'

    const fakerequest = sinon.fake.resolves({id:userID,name:'test'});
    sinon.replace(usersService, 'getUser', fakerequest)

    return agent.get('/users/' + userID)
      .expect('Content-Type', 'application/json; charset=utf-8')
      .expect((req) => {
        expect(fakerequest).to.have.been.calledWith(userID)
      })
      .expect(200,{id:userID,name:'test'})
  })

  // it('upload image',()=>{
    // .send()
    // .attach('image','plik_z_pania.png',{})
  // })

});