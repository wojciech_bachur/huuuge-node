// import { Router } from "express";
import Router from "express-promise-router";
import { usersService } from '../services'
// import url from 'url'
// url.parse(req.url).path

import fs from 'fs'
import path from 'path'

import multer from 'multer'
import { NotFoundError } from "../services/errors";
const photoUpload = multer({
  dest: './public/userphotos/',
})

export const usersRoutes = Router()
  // .param(":user_id", (req, res, next) => { })

  .get("/", async (req, res) => {
    
    const result = await usersService.getUsers();
    res.json(result);
  })

  .get("/:user_id", async (req, res, next) => {
    
    try {
      const { user_id } = req.params;
      res.json(await usersService.getUser(user_id));
    } catch (err) { next(err) }
  })

  .get('/:user_id/image', (req, res) => {
    // res.sendFile('ryan-arnst-e_tXcjm0LzQ-unsplash.jpg', {
    res.download('./public/ryan-arnst-e_tXcjm0LzQ-unsplash.jpg', 'dziendobry.jpg', {})
  })

  .post<{ user_id: string }>('/:user_id/image', photoUpload.single('image'), async (req, res) => {
    const { user_id } = req.params

    const user = await usersService.getUser(user_id)
    if (!user) {
      throw new NotFoundError('User doesn`t exists')
    }

    const { destination, filename, originalname } = req.file//.destination
    await fs.promises.mkdir(path.join(destination, user_id), {
      recursive: true
    })
    const userImagePath = path.join(destination, user_id, originalname);
    await fs.promises.rename(
      path.join(destination, filename),
      userImagePath,
    )
    user.image = userImagePath
    const result = await usersService.updateUser(user);

    res.send(result);
    // req.pipe(res)
  })

  .post("/", async (req, res, next) => {
    const body = req.body;

    const exist = await usersService.userExists(body.id);

    if (exist !== -1) {
      res.sendStatus(409).json({ message: 'User already exists' })
    }
    const result = await usersService.saveUser(body);

    res.json(result);
  })

  .put("/:user_id", async (req, res, next) => {
    const body = req.body;
    const existIndex = await usersService.userExists(body.id);

    if (existIndex == -1) {
      next(new Error('User doesn`t exists'))
    } else {
      const result = await usersService.updateUser(body);
      res.send(result);
    }
  })
