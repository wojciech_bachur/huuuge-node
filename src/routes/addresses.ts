import { Router } from "express";


export const addressesRoutes = Router()

    .get('/', (req, res) => {
        res.send(
            [
                {id: 1, userId: 1, street: "Fordonska 1"},
                {id: 2, userId: 2, street: "Fordonska 2"}
            ]
        )
    })
