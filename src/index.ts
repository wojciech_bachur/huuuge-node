import express, { RequestHandler, Request, Response, NextFunction } from 'express'
import { configure } from './config/configure';
import routes from './routes';
import { NotFoundError } from "./services/errors";

// let RedisStore = require('connect-redis')(session)
// let redisClient = redis.createClient()

const app = express();
configure(app)

app.use('/public', express.static('./public', {}))

app.use(routes);

app.use((error: any, req: Request, res: Response, next: NextFunction) => {
  if (error instanceof NotFoundError) {
    res.sendStatus(404)
  }
  next(error)
})

app.listen(parseInt(process.env.PORT!), process.env.HOST!, () => {
  console.log(`Listening on http://${process.env.HOST}:${process.env.PORT}/`)
});
