import requestId from 'express-request-id';
import cors from 'cors';
import helmet from 'helmet';
import errorhandler from 'errorhandler';
import morgan from 'morgan';
import connectTimeout from 'connect-timeout';
import cookieParser from 'cookie-parser';
import session from 'express-session';
import { Application, json } from 'express'
import passport from 'passport'

import './configureAuth'

export const configure = (app: Application) => {
  // app.use(express.urlencoded()) // forms
  app.set('trust proxy', 1);

  process.env.NODE_ENV === 'production' && app.use(connectTimeout('5s', {}));
  app.use(errorhandler());
  app.use(cors({}));
  app.use(helmet());
  app.use(requestId({}));
  app.use(json({
    // limit:'100kb',
  }))
  app.use(morgan('combined', {}));
  app.use(cookieParser('alamakota', {}));
  app.use(session({
    secret: 'alamakota',
    name: 'SessionID',
    resave: false,
    rolling: true,
    saveUninitialized: true,
    cookie: { sameSite: 'none' /* , ... */ }
    // store: new RedisStore({ client: redisClient }),
  }));

  app.use(passport.initialize())
  app.use(passport.session())

}

