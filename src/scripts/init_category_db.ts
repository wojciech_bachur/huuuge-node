import "reflect-metadata";
import { createConnection } from "typeorm";
import { Category } from "../entity/Category";

createConnection().then(async connection => {

  console.log("Inserting a new categories into the database...");
  const categories = Array.apply(null, Array(40))
    .map(() => new Category())
    .map((category: Category, i) => {
      category.name = `Category ${++i}`
      return category;
    });

  await connection.manager.save(categories);
  console.log("Saved a new categories with ids: " + JSON.stringify(categories.map(x => x.id)));

  console.log("Loading categories from the database...");
  const users = await connection.manager.find(Category);
  console.log("Loaded categories: ", users);

  console.log("Here you can setup and run express/koa/any other framework.");

}).catch(error => console.log(error));
