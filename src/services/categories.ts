

const mockCategories = [
  { id: 1, name: "Vegetables" },
  { id: 2, name: "Fruits" },
  { id: 3, name: "Clothes" },
  { id: 4, name: "Tools" },
];


export class CategoriesService {

  private categories = mockCategories;

  async updateCategory(body: { id: number; name: string; }) {
    const category = await this.getCategory(body.id);

    if(category) {
      category.name = body.name;
      return true;
    }

    return false;
  }

  async saveCategory(body: { id: number; name: string; }) {
    const categoryExists = await this.categoryExists(body.id);
    if(categoryExists) {
      return false;
    }

    this.categories.push(body);
    return true;
  }


  async getCategories() {
    return this.categories;
  }

  async getCategory(categoryId: number) {
    return this.categories.find(
      (u) => u.id == categoryId
    )
  }

  async categoryExists(categoryId: number) {
    return this.categories.findIndex(
      (u) => u.id == categoryId
    ) > -1;
  }

}

