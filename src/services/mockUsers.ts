import { User } from "../interfaces/User";

export const mockUsers: User[] = [
  { id: '1', name: "User 1" },
  { id: '2', name: "User 2" },
];
